import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import UserList from "./src/list";
import UserDetails from "./src/details";

const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="List">
        <Stack.Screen name="List" component={UserList}/>
        <Stack.Screen name="Details" component={UserDetails}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}


